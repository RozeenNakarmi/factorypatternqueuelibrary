using FactoryPatternQueue;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QueueTest
{
    [TestClass]
    public class LimitTo10Test
    {
        [TestMethod]
        public void LimitTo10_Scenario_Enqueue_CheckingListValue()
        {
            //Act
            var limitTo10 = new LimitTo10();

            //Arrange
            List<string> lst = new List<string> {"1", "2","3","4","5","6","7","8","9"};
            foreach(var values in lst)
            {
                limitTo10.Enqueue(values);
            }
            //Assert
            Assert.AreEqual(null, limitTo10.Placement(9));
        }

        [TestMethod]
        public void LimitTo10_Scenario_Enqueue_CountingValue()
        {
            //Act
            var limitTo10 = new LimitTo10();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", null };
            foreach (var values in lst)
            {
                limitTo10.Enqueue(values);
            }

            //Assert
            Assert.AreEqual(lst.Count, limitTo10.List().Count());

        }
        [TestMethod]
        public void LimitTo10_Scenario_DequeuingValue()
        {
            //Act
            var limitTo10 = new LimitTo10();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
            foreach (var values in lst)
            {
                limitTo10.Enqueue(values);

            }
            limitTo10.Dequeue();

            //Assert
            Assert.AreEqual("2", limitTo10.Placement(0));

        }
        [TestMethod]
        public void LimitTo10_Scenario_PrintValue()
        {
            //Act
            var limitTo10 = new LimitTo10();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
            foreach (var values in lst)
            {
                limitTo10.Enqueue(values);

            }
            limitTo10.QueuePrint();

            //Assert
            Assert.AreEqual(limitTo10.List().Count(), limitTo10.TestPrint().Count());

        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void LimitTo10_Scenario_Enqueue_ExceptionOverPopullatedTest()
        {
            //Act
            var limitTo10 = new LimitTo10();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
            foreach (var values in lst)
            {
                limitTo10.Enqueue(values);
              
            }
            limitTo10.Enqueue("11");
        }

        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void LimitTo10_Scenario_ExceptionPrintingEmptyQueue()
        {
            var limitTo10 = new LimitTo10();

            limitTo10.QueuePrint();
        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void LimitTo10_Scenario_ExceptionDequeuingEmptyValue()
        {
            var limitTo10 = new LimitTo10();

            limitTo10.Dequeue();
        }
    }
    [TestClass]
    public class InfiniteTest
    {
        [TestMethod]
        public void Infinite_Scenario_Enqueue_CheckingListValue()
        {
            var infinite = new Infinite();
            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            foreach (var values in lst)
            {
                infinite.Enqueue(values);
            }
            //Assert
            Assert.AreEqual("1", infinite.Placement(0));
        }

        [TestMethod]
        public void Infinite_Scenario_Enqueue_CountingValue()
        {
            //Act
            var infinite = new Infinite();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", null };
            foreach (var values in lst)
            {
                infinite.Enqueue(values);
            }

            //Assert
            Assert.AreEqual(lst.Count, infinite.List().Count());
        }

        [TestMethod]
        public void Infinite_Scenario_DequeuingValue()
        {
            //Act
            var infinite = new Infinite();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
            foreach (var values in lst)
            {
                infinite.Enqueue(values);

            }
            infinite.Dequeue();

            //Assert
            Assert.AreEqual("2", infinite.Placement(0));
        }
        [TestMethod]
        public void Infinite_Scenario_PrintValue()
        {
            //Act
            var infinite = new Infinite();

            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10","11","22" };
            foreach (var values in lst)
            {
                infinite.Enqueue(values);

            }
            infinite.QueuePrint();

            //Assert
            Assert.AreEqual(infinite.List().Count(), infinite.TestPrint().Count());
        }

        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void Infinite_Scenario_ExceptionPrintingEmptyQueue()
        {
            //Act
            var infinite = new Infinite();

            infinite.QueuePrint();
        }

        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void Infinite_Scenario_ExceptionDequeingEmptyQueue()
        {
            //Act
            var infinite = new Infinite();

            infinite.Dequeue();
        }
    }
    [TestClass]
    public class UserDefineTest
    {
        [TestMethod]
        public void UserDefine_Scenario_Enqueue_CoutingValue()
        {
            var userdefine = new UserDefine(5);
            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5" };
            foreach (var values in lst)
            {
                userdefine.Enqueue(values);
            }

            //Assert
            Assert.AreEqual(lst.Count, userdefine.List().Count());

        }
        [TestMethod]
        public void UserDefine_Scenario_Enqueue_CheckingListValue()
        {
            var userdefine = new UserDefine(5);
            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5" };
            foreach (var values in lst)
            {
                userdefine.Enqueue(values);
            }
            //Assert
            Assert.AreEqual("1", userdefine.Placement(0));
        }
        [TestMethod]
        public void UserDefine_Scenario_DequeueValue()
        {
            var userdefine = new UserDefine(5);
            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5" };
            foreach (var values in lst)
            {
                userdefine.Enqueue(values);
            }
            userdefine.Dequeue();
            //Assert
        }
        [TestMethod]
        public void UserDefine_Scenario_PrintValue()
        {
            var userdefine = new UserDefine(5);
            List<string> lst = new List<string> { "1", "2", "3", "4", "5" };
            foreach (var values in lst)
            {
                userdefine.Enqueue(values);
            }
            userdefine.QueuePrint();
            Assert.AreEqual(userdefine.List().Count(),userdefine.TestPrint().Count());

        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void UserDefine_Scenario_ExceptionPrintingEmptyQueue()
        {
            var userdefine = new UserDefine(5);
            userdefine.QueuePrint();

        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void UserDefine_Scenario_ExceptionDequeingEmptyValue()
        {
            var userdefine = new UserDefine(5);

            userdefine.Dequeue();
        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void UserDefine_Scenario_ExceptionOverPopullated()
        {
            var userdefine = new UserDefine(5);
            //Arrange
            List<string> lst = new List<string> { "1", "2", "3", "4", "5","6" };
            foreach (var values in lst)
            {
                userdefine.Enqueue(values);
            }
            
        }

    }

    [TestClass]
    public class GenericInfiniteTest
    {
        [TestMethod]
        public void GenericInfinite_Scenario_Enqueue_CountingValue()
        {
            var genericInfinite = new GenericInfinite<int>();
            List<int> lst = new List<int> { 1,2,3,4,5,6,7,8,9,10,11,12,13};
            foreach (var values in lst)
            {
                genericInfinite.Enqueue(values);
            }
            Assert.AreEqual(lst.Count(), genericInfinite.List().Count());
        }
        [TestMethod]
        public void GenericInfinite_Scenario_Enqueue_CheckingListValue()
        {
            var genericInfinite = new GenericInfinite<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            foreach (var values in lst)
            {
                genericInfinite.Enqueue(values);
            }
            Assert.AreEqual(5, genericInfinite.Placement(4));
        }
        [TestMethod]
        public void GenericInfinite_Scenario_DequeueValue()
        {
            var genericInfinite = new GenericInfinite<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            foreach (var values in lst)
            {
                genericInfinite.Enqueue(values);
            }
            genericInfinite.Dequeue();
            Assert.AreEqual(2, genericInfinite.Placement(0));
        }

        [TestMethod]
        public void GenericInfinite_Scenario_PrintValue()
        {
            var genericInfinite = new GenericInfinite<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            foreach (var values in lst)
            {
                genericInfinite.Enqueue(values);
            }
            genericInfinite.QueuePrint();
            Assert.AreEqual(genericInfinite.List().Count(), genericInfinite.TestPrint().Count);
        }

        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericInfinite_Scenario_ExceptionPrintingEmptyQueue()
        {
            var genericInfinite = new GenericInfinite<int>();

            genericInfinite.QueuePrint();
        }

        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericInfinite_Scenario_ExceptionDequeingEmptyValue()
        {
            var genericInfinite = new GenericInfinite<int>();
            genericInfinite.Dequeue();

        }
    }
    [TestClass]
    public class GenericLimitTo10Test
    {
        [TestMethod]
        public void GenericLimitTo10_Scenario_Enqueue_CheckingListValue()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericLimitTo10.Enqueue(values);
            }
            Assert.AreEqual(1, genericLimitTo10.Placement(0));
        }

        [TestMethod]
        public void GenericLimitTo10_Scenario_Enqueue_CountValue()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericLimitTo10.Enqueue(values);
            }
            Assert.AreEqual(lst.Count(), genericLimitTo10.List().Count());
        }

        [TestMethod]
        public void GenericLimitTo10_Scenario_DequeueingValue()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericLimitTo10.Enqueue(values);
            }
            genericLimitTo10.Dequeue();
            Assert.AreEqual(2, genericLimitTo10.Placement(0));
        }

        [TestMethod]
        public void GenericLimitTo_Scenario_PrintValue()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericLimitTo10.Enqueue(values);
            }
            genericLimitTo10.QueuePrint();
            Assert.AreEqual(genericLimitTo10.List().Count(), genericLimitTo10.TestPrint().Count());
        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericLimitTo_Scenario_ExceptionOverPopullated()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericLimitTo10.Enqueue(values);
            }
            genericLimitTo10.Enqueue(1);
        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericLimitTo_Scenario_ExceptionPrintingEmptyQueue()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();

            genericLimitTo10.QueuePrint();
        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericLimitTo_Scenario_ExceptionPrintingDequeingEmptyValue()
        {
            var genericLimitTo10 = new GenericLimitTo10<int>();

            genericLimitTo10.Dequeue();

        }

    }
    [TestClass]
    public class GenericUserDefine
    {
        [TestMethod]
        public void GenericUserDefine_Scenario_Enqueue_CheckingListValue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericUserDefine.Enqueue(values);
            }
            Assert.AreEqual(1, genericUserDefine.Placement(0));
        }
        [TestMethod]
        public void GenericUserDefine_Scenario_Enqueue_CountingValue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericUserDefine.Enqueue(values);
            }
            Assert.AreEqual(10, genericUserDefine.List().Count());
        }
        [TestMethod]
        public void GenericUserDefine_Scenario_Enqueue_DequeueValue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericUserDefine.Enqueue(values);
            }
            genericUserDefine.Dequeue();
            Assert.AreEqual(2, genericUserDefine.Placement(0));
        }
        [TestMethod]
        public void GenericUserDefine_Scenario_EnqueuingValue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            genericUserDefine.Enqueue(1);
            Assert.AreEqual(1, genericUserDefine.Placement(0));
        }
        [TestMethod]
        public void GenericUserDefine_Scenario_PrintValue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (var values in lst)
            {
                genericUserDefine.Enqueue(values);
            }
            genericUserDefine.QueuePrint();
            Assert.AreEqual(genericUserDefine.List().Count(), genericUserDefine.TestPrint().Count());
        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericUserDefine_Scenario_ExceptionOverpopullated()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            List<int> lst = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            foreach (var values in lst)
            {
                genericUserDefine.Enqueue(values);
            }

        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericUserDefine_Scenario_ExceptionPrintingEmptyQueue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            genericUserDefine.QueuePrint();

        }
        [TestMethod]
        [ExpectedException(typeof(CreatedException))]
        public void GenericUserDefine_Scenario_ExceptionDequeingEmptyQueue()
        {
            var genericUserDefine = new GenericUserDefine<int>(10);
            genericUserDefine.Dequeue();

        }

    }
}
