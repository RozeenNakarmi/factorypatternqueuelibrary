﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace FactoryPatternQueue
{
    public class CreatedException: Exception
    {
        public CreatedException()
            :base()
        {

        }
        public CreatedException(string message)
            :base(message)
        {

        }
        public CreatedException(string message, Exception exception)
            :base(message, exception)
        {

        }
        public CreatedException(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }
    }
}
