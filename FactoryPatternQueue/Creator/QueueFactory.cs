﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue.Creator
{
    public abstract class QueueFactory
    {
        public abstract IQueue<string> GetInfinite();
        public abstract IQueue<string> GetLimitTo10();
        public abstract IQueue<string> GetUserDefine();
        public abstract IQueue<T> GetGenericInfinite<T>(string type);
        public abstract IQueue<T> GetGenericFinite<T>(string type);
        public abstract IQueue<T> GetGenericUserDefine<T>(string type);

    }
}
