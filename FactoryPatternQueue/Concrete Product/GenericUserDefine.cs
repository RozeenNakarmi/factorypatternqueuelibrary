﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue
{
    public class GenericUserDefine<T> : IQueue<T>
    {
        private readonly T[] _arr;
        private readonly int _front;
        private int back;
        private readonly int _max;
        private readonly List<T> _prntList = new List<T>();

        public GenericUserDefine(int size)
        {
            _arr = new T[size];
            _front = 0;
            back = -1;
            _max = size;
        }

        public void Dequeue()
        {
            if (_front == back + 1)
            {
                throw new CreatedException("Queue is empty");
            }
            else
            {
                Console.WriteLine("deleted element is: " + _arr[_front]);
                for (int i = 0; i < back - 1; i++)
                {
                    _arr[i] = _arr[i + 1];
                }
                back--;
            }
        }

        public void Enqueue(T item)
        {
            if (back == _max - 1)
            {
                throw new CreatedException("Queue are overpopullated");

            }
            else
            {
                _arr[++back] = (item);
            }
        }

        public void QueuePrint()
        {

            if (_front == back + 1)
            {
                throw new CreatedException("Queue is empty.");
            }
            else
            {
                for (int i = _front; i <= back; i++)
                {
                    Console.WriteLine("Item[" + (i + 1) + "]: " + _arr[i]);
                    _prntList.Add(_arr[i]);
                }
            }
        }
        public int Placement(int valuePlacement)
        {
            return int.Parse((_arr[valuePlacement]).ToString());
        }
        public T[] List()
        {
            return _arr;
        }
        public List<T> TestPrint()
        {
            return _prntList;
        }
    }
}
