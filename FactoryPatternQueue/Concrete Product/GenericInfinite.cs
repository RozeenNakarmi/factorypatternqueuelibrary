﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue
{
    public class GenericInfinite<T> : IQueue<T>
    {
        private readonly int _front;
        private int _back;
        private readonly List<T> _listInfinite;
        private readonly List<T> _prntList = new List<T>();


        public GenericInfinite()
        {
            _listInfinite = new List<T>();
            _front = 0;
            _back = -1;
        }

        public void Dequeue()
        {

                if (_front == _back + 1)
                {
                    
                    throw new CreatedException("Queue is empty");
                }
                else
                {
                    Console.WriteLine("deleted element is: " + _listInfinite[_front]);
                    for (int i = 0; i < _back - 1; i++)
                    {
                        _listInfinite[i] = _listInfinite[i + 1];
                    }
                    _back--;
                }
        }

        public void Enqueue(T item)
        {
            _listInfinite.Add(item);
            _back++;
        }

        public void QueuePrint()
        {

            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is empty");
            }
            else
            {
                for (int i = _front; i <= _back; i++)
                {
                    Console.WriteLine("Item[" + (i + 1) + "]: " + _listInfinite[i]);
                    _prntList.Add(_listInfinite[i]);
                }
            }
        }
        public int Placement(int valuePlacement)
        {
            return int.Parse( (_listInfinite[valuePlacement]).ToString());
        }
        public List<T> List()
        {
            return _listInfinite;
        }
        public List<T> TestPrint()
        {
            return _prntList;
        }
    }
}
