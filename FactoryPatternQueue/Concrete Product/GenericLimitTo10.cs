﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue
{
    public class GenericLimitTo10<T> : IQueue<T>
    {
        private readonly T[] _arr;
        private readonly int _front;
        private int _back;
        private readonly int _max;
        private readonly List<T> _prntList = new List<T>();

        public GenericLimitTo10()
        {
            _arr = new T[10];
            _front = 0;
            _back = -1;
            _max = 10;
        }
        public void Dequeue()
        {

            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is Empty");
            }
            else
            {
                Console.WriteLine("deleted element is: " + _arr[_front]);
                for (int i = 0; i < _back - 1; i++)
                {
                    _arr[i] = _arr[i + 1];
                }
                _back--;
            }
        }

        public void Enqueue(T item)
        {

            if (_back == _max - 1)
            {
                throw new CreatedException("Data are overpopullated"); 
            }
            else
            {
                _arr[++_back] = (item);
            }
        }
        public void QueuePrint()
        {
            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is Empty");
            }
            else
            {
                for (int i = _front; i <= _back; i++)
                {
                    Console.WriteLine("Item[" + (i + 1) + "]: " + _arr[i]);
                    _prntList.Add(_arr[i]);

                }
            }
        }
        public int Placement(int valuePlacement)
        {
            return int.Parse((_arr[valuePlacement]).ToString());
        }
        public T[] List()
        {
            return _arr;
        }
        public List<T> TestPrint()
        {
            return _prntList;
        }
    }
}
