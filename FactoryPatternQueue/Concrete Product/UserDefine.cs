﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue
{
    public class UserDefine : IQueue<String>
    {
        private readonly string[] _arr;
        private readonly int _front;
        private int _back;
        private readonly int _max;
        private readonly List<string> _prntList = new List<string>();


        public UserDefine(int size)
        {
            _arr = new string[size];
            _front = 0;
            _back = -1;
            _max = size;
        }

        public void Dequeue()
        {
            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is empty.");
            }
            else
            {
                Console.WriteLine("deleted element is: " + _arr[_front]);
                for (int i = 0; i < _back - 1; i++)
                {
                    _arr[i] = _arr[i + 1];
                }
                _back--;
            }
        }

        public void Enqueue(string item)
        {
            if (_back == _max - 1)
            {
                throw new CreatedException("Queue is out of bound.");

            }
            else
            {
                _arr[++_back] = (item);
            }       
        }
        public void QueuePrint()
        {
           
            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is empty.");
            }
            else
            {
                for (int i = _front; i <= _back; i++)
                {
                    Console.WriteLine("Item[" + (i + 1) + "]: " + _arr[i]);
                    _prntList.Add(_arr[i]);
                }
            }
        }
        public string Placement(int valuePlacement)
        {
            return _arr[valuePlacement];
        }

        public string[] List()
        {
            return _arr;
        }

        public List<string> TestPrint()
        {
            return _prntList;
        }
    }
}
