﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue
{
    public class Infinite : IQueue<String>
    {
        private readonly int _front;
        private int _back;
        private readonly List<string> _listInfinite;
        private readonly List<string> _prntList = new List<string>();

        public Infinite()
        {
            _listInfinite = new List<string>();
            _front = 0;
            _back = -1;
        }

        public void Dequeue()
        {

            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is Empty");
            }
            else
            {
                Console.WriteLine("deleted element is: " + _listInfinite[_front]);
                for (int i = 0; i < _back - 1; i++)
                {
                    _listInfinite[i] = _listInfinite[i + 1];
                }
                _back--;
            }
        }

        public void Enqueue(string item)
        {
            _listInfinite.Add(item);
            _back++;
        }

        public void QueuePrint()
        {

            if (_front == _back + 1)
            {
                throw new CreatedException("Queue is Empty");
            }
            else
            {
                for (int i = _front; i <= _back; i++)
                {
                    Console.WriteLine("Item[" + (i + 1) + "]: " + _listInfinite[i]);
                    _prntList.Add(_listInfinite[i]);
                }
            }
        }
        public string Placement(int valuePlacement)
        {
            return _listInfinite[valuePlacement];
        }
        public List<string> List()
        {
            return _listInfinite;
        }
        public List<string> TestPrint()
        {
            return _prntList;
        }
    }
}
