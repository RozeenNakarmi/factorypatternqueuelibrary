﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue
{
    public interface IQueue<T>
    {
        void Enqueue(T item);
        void Dequeue();
        void QueuePrint();

    }
}
