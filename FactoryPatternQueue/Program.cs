﻿using FactoryPatternQueue.ConcreteCreator;
using FactoryPatternQueue.Creator;
using System;

namespace FactoryPatternQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Select Queue from below \n 1.Finite length of size 10 of string type \n 2.User defined Size of string type \n 3.Infinite length of string type " +
                "\n 4.Finite length of size 10 of generic type \n 5.User defined Size of Generictype \n 6.Infinite length of generic type");
            int item = 0;
            try
            {
                 item = int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("You have entered string value.\nThe Program will close now.");
            }
            finally
            {
                QueueFactory queueFactory = new ConcreteCreatorFactory();
                switch (item)
                {
                    //String Finite
                    case 1:
                        IQueue<string> queueLimitTo10 = queueFactory.GetLimitTo10();

                        for (int i = 0; i < 10; i++)
                        {
                            queueLimitTo10.Enqueue("dsadas");

                        }
                        queueLimitTo10.QueuePrint();
                        break;
                    //String User Define
                    case 2:
                        IQueue<string> queueUserDefine = queueFactory.GetUserDefine();

                        for (int i = 0; i < 15; i++)
                        {
                            queueUserDefine.Enqueue("dsadas");

                        }
                        queueUserDefine.QueuePrint();
                        break;
                    //String Infinite length
                    case 3:
                        IQueue<string> queueInfinite = queueFactory.GetInfinite();

                        for (int i = 0; i < 15; i++)
                        {
                            queueInfinite.Enqueue("dsadas");

                        }
                        queueInfinite.QueuePrint();
                        break;
                    //String Finite Generic
                    case 4:
                        IQueue<int> queueGenericFinite = queueFactory.GetGenericFinite<int>("int");

                        for (int i = 0; i < 15; i++)
                        {
                            queueGenericFinite.Enqueue(2);

                        }
                        queueGenericFinite.QueuePrint();
                        break;


                    //String User Define Generic
                    case 5:
                        IQueue<int> queueGenericUserDefine = queueFactory.GetGenericUserDefine<int>("int");

                        for (int i = 0; i < 15; i++)
                        {
                            queueGenericUserDefine.Enqueue(2);

                        }
                        queueGenericUserDefine.QueuePrint();
                        break;
                    //String Infinite Generic
                    case 6:
                        IQueue<int> queueGenericInfinite = queueFactory.GetGenericInfinite<int>("int");

                        for (int i = 0; i < 15; i++)
                        {
                            queueGenericInfinite.Enqueue(2);

                        }
                        queueGenericInfinite.QueuePrint();
                        break;

                    default:
                        break;
                }
            }

        }
    }
}
