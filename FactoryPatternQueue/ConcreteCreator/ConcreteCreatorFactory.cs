﻿using FactoryPatternQueue.Creator;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPatternQueue.ConcreteCreator
{
    public class ConcreteCreatorFactory : QueueFactory
    {
        public override IQueue<T> GetGenericFinite<T>(string type)
        {
            try
            {
                switch (type)
                {
                    case "string":
                        return (IQueue<T>)new GenericLimitTo10<string>();
                    case "int":
                        return (IQueue<T>)new GenericLimitTo10<int>();
                    default:
                        throw new NotImplementedException();
                }
            }
            catch 
            {
                throw new NotImplementedException();
            }
        }

        public override IQueue<T> GetGenericInfinite<T>(string type)
        {
            try
            {
                switch (type)
                {
                    case "string":
                        return (IQueue<T>) new GenericInfinite<string>();
                    case "int":
                        return (IQueue<T>) new GenericInfinite<int>();
                    default:
                        throw new NotImplementedException();
                }
                 
            }
            catch
            {
                throw new NotImplementedException();

            }
        }

        public override IQueue<T> GetGenericUserDefine<T>(string type)
        {
            try
            {
                Console.WriteLine("How many times do you want to define the queue");
                int t = Int32.Parse(Console.ReadLine());
                switch (type)
                {
                    case "string":
                        return (IQueue<T>)new GenericUserDefine<string>(t);
                    case "int":
                        return (IQueue<T>)new GenericUserDefine<int>(t);
                    default:
                        throw new NotImplementedException();
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public override IQueue<string> GetInfinite()
        {
            try
            {
                return new Infinite();
            }
            catch
            {
                throw new NotImplementedException();

            }
        }

        public override IQueue<string> GetLimitTo10()
        {
            try
            {
                return new LimitTo10();
            }
            catch
            {
                throw new NotImplementedException();

            }
        }

        public override IQueue<string> GetUserDefine()
        {
            try
            {
                Console.WriteLine("How many times do you want to define the queue");
                int type = Int32.Parse(Console.ReadLine());
                return new UserDefine(type);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }
    }
}
